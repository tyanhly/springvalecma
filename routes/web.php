<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('api')->group(function () {
    Route::get('events', 'EventController@index');
    Route::get('upcomming-events', 'EventController@upcommingEvents');
    Route::get('events/{uri}', 'EventController@show');
    Route::get('seremons', 'SeremonController@index');
    Route::get('seremons/{uri}', 'SeremonController@show');
    Route::get('testimony-stories', 'TestimonyStoryController@index');
    Route::get('testimony-stories/{uri}', 'TestimonyStoryController@show');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('{page}', function () {
    return view('welcome');
});

Route::get('{page}/{uri}', function () {
    return view('welcome');
});