<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Seremon;
use App\Http\Resources\Seremon as SeremonResource;
use App\Http\Resources\Seremons as SeremonsResource;
class SeremonController extends Controller
{
    /**
     * @param Request $request
     * @return SeremonsResource
     */
    public function index(Request $request)
    {
        $perPage = $request->input('per_page',5);
        $seremons = new SeremonsResource(Seremon::where('publish_flg', true)->where('publish_at', '<' ,now())->orderBy('publish_at','desc')->paginate($perPage));
        return $seremons;
    }

    /**
     * @param $uri
     * @return SeremonResource
     */
    public function show($uri)
    {
        $seremon = new SeremonResource(Seremon::where('uri', '=', $uri)->where('publish_flg', true)->first());
//        var_dump($seremon->jsonSerialize());die;
        return $seremon;

    }

}
