<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\TestimonyStory;
use App\Http\Resources\TestimonyStory as TestimonyStoryResource;
use App\Http\Resources\TestimonyStories as TestimonyStoriesResource;
class TestimonyStoryController extends Controller
{
    /**
     * @param Request $request
     * @return TestimonyStoriesResource
     */
    public function index(Request $request)
    {
        $perPage = $request->input('per_page',5);
        $rows = new TestimonyStoriesResource(
            TestimonyStory::where('publish_flg', true)->orderBy('created_at','desc')->paginate($perPage)
        );
        return $rows;
    }

    /**
     * @param $uri
     * @return TestimonyStoryResource
     */
    public function show($uri)
    {
        $row = new TestimonyStoryResource(
            TestimonyStory::where('uri', '=', $uri)->where('publish_flg', '=', '1')->first()
        );
        return $row;
    }

}
