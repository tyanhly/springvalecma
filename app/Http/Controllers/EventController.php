<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Event;
use App\Http\Resources\Event as EventResource;
use App\Http\Resources\Events as EventsResource;
class EventController extends Controller
{
    /**
     * @param Request $request
     * @return EventsResource
     */
    public function index(Request $request)
    {
        $perPage = $request->input('per_page',5);
        $events = new EventsResource(Event::where('publish_flg', true)->orderBy('event_time','desc')->paginate($perPage));
        return $events;
    }

    /**
     * @param Request $request
     * @return EventsResource
     */
    public function upcommingEvents(Request $request)
    {
        $perPage = $request->input('per_page',4);
        $events = new EventsResource(
            Event::where('publish_flg', true)->where('event_time', '>', now())
                ->orderBy('event_time','asc')->paginate(4));
        return $events;
    }

    /**
     * @param $uri
     * @return EventResource
     */
    public function show($uri)
    {
        $events = new EventResource(Event::where('uri', '=', $uri)->where('publish_flg', true)->first());
        return $events;

    }

}
