<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TestimonyStory extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'uri' => $this->uri,
            'title' => $this->title,
            'content' => $this->content,
            'desc' => $this->desc,
            'author_name' => $this->author_name,
            'author_email' => $this->author_email,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
        ];
    }
}
