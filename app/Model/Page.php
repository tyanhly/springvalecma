<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\SoftDeletes;
class Page extends Model
{

    use SoftDeletes;
    protected $fillable = ['name', 'content'];
    protected $dates = ['deleted_at'];
}
