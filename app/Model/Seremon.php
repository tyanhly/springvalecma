<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\SoftDeletes;
class Seremon extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
