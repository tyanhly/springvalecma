<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resource('events', EventController::class);
    $router->resource('seremons', SeremonController::class);
    $router->resource('pages', PageController::class);
    $router->resource('issues', IssueController::class);
    $router->resource('testimony-stories', TestimonyStoryController::class);

});
