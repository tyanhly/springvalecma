<?php

use Encore\Admin\Form;
use App\Admin\Extensions\Form\CKEditor;
use App\Admin\Extensions\Form\Script;

Encore\Admin\Form::forget(['map', 'editor']);

Form::extend('datetime_minute', \App\Admin\Extensions\Form\DatetimeMinute::class);
Form::extend('ckeditor', CKEditor::class);
Form::extend('script', Script::class);
Form::extend('input_checkbox', \App\Admin\Extensions\Form\InputCheckbox::class);