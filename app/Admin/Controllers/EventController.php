<?php

namespace App\Admin\Controllers;

use App\Model\Event;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class EventController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Event');
            $content->description('list');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Event');
            $content->description('edit');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Event');
            $content->description('create');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Event::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->title(ucfirst(trans('title')))->sortable();
            $grid->event_time(ucfirst(trans('event_time')))->sortable();
            $grid->publish_flg(ucfirst(trans('published')))->sortable();
            $grid->created_at(ucfirst(trans('created_at')))->sortable();
            $grid->updated_at(ucfirst(trans('updated_at')))->sortable();
            $grid->filter(function ($filter) {
                $filter->like('title', ucfirst(trans('title')));
                $filter->like('desc', ucfirst(trans('desc')));
                $filter->like('content', ucfirst(trans('content')));
                $filter->equal('publish_flg', ucfirst(trans('publish_flg')));
                $filter->between('event_time', ucfirst(trans('event_time')))->datetime(['format' => 'YYYY-MM-DD']);
                $filter->between('created_at', ucfirst(trans('created_at')))->datetime(['format' => 'YYYY-MM-DD']);
                $filter->between('updated_at', ucfirst(trans('updated_at')))->datetime(['format' => 'YYYY-MM-DD']);
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Event::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('title', ucfirst(trans('title')))->rules('required');
            $form->text('uri', ucfirst(trans('uri')));
            $form->datetime_minute('event_time', ucfirst(trans('event_time')))->rules('required');
            $form->textarea('desc', ucfirst(trans('description')))->rules('required');

            $form->script('page.events.js');

            $form->ckeditor('content', ucfirst(trans('content')))->rules('required');
            $form->switch('public_flg', ucfirst(trans('public_flg')))->rules('required');

            $form->display('created_at', ucfirst(trans('created_at')));
            $form->display('updated_at', ucfirst(trans('updated_at')));
            
            $form->saving(function (Form $form) {
                if($form->uri == ''){
                    $form->uri = strtolower(str_replace(' ', '-',
                        preg_replace("/[^a-zA-Z0-9\s\-]/u", '', $form->title)));
                }
            });
        });
    }
}
