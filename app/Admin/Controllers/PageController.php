<?php

namespace App\Admin\Controllers;

use App\Model\Page;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class PageController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Page');
            $content->description('list');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Page');
            $content->description('edit');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Page');
            $content->description('create');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Page::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->name(ucfirst(trans('name')))->sortable();
            $grid->uri(ucfirst(trans('page.uri')))->sortable();

            $grid->created_at();
            $grid->updated_at();

            $grid->filter(function ($filter) {
                $filter->like('name', ucfirst(trans('name')));
                $filter->like('content', ucfirst(trans('content')));
                $filter->between('created_at', ucfirst(trans('created_at')))->datetime(['format' => 'YYYY-MM-DD']);
                $filter->between('updated_at', ucfirst(trans('updated_at')))->datetime(['format' => 'YYYY-MM-DD']);
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Page::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', ucfirst(trans('name')))->rules('required');
            $form->text('uri', ucfirst(trans('page.uri')))->rules('required');
            $form->script('page.pages.js');
            $form->ckeditor('content', ucfirst(trans('content')));

            $form->display('created_at', ucfirst(trans('created_at')));
            $form->display('updated_at', ucfirst(trans('updated_at')));

            $form->saving(function (Form $form) {
                if($form->uri == ''){
                    $form->uri = strtolower(str_replace(' ', '-', $form->name));
                }
            });
        });
    }
}
