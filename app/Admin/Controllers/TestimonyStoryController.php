<?php

namespace App\Admin\Controllers;

use App\Model\TestimonyStory;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class TestimonyStoryController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Testimony TestimonyStories');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Testimony TestimonyStories');
            $content->description('Edit');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(trans('Testimony Stories'));
            $content->description('Create');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(TestimonyStory::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->title(ucfirst(trans('title')))->sortable();
            $grid->author_name(ucfirst(trans('author_name')))->sortable();
            $grid->author_email(ucfirst(trans('author_email')))->sortable();
            $grid->publish_flg(ucfirst(trans('published')))->sortable();
            $grid->created_at(ucfirst(trans('created_at')))->sortable();
            $grid->updated_at(ucfirst(trans('updated_at')))->sortable();

            $grid->filter(function ($filter) {
                $filter->like('auth_name', ucfirst(trans('auth_name')));
                $filter->equal('author_email', ucfirst(trans('author_email')));
                $filter->like('title', ucfirst(trans('title')));
                $filter->like('content', ucfirst(trans('content')));
                $filter->like('desc', ucfirst(trans('description')));
                $filter->equal('publish_flg', ucfirst(trans('publish_flg')));
                $filter->between('created_at', ucfirst(trans('created_at')))->datetime(['format' => 'YYYY-MM-DD']);
                $filter->between('updated_at', ucfirst(trans('updated_at')))->datetime(['format' => 'YYYY-MM-DD']);
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(TestimonyStory::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('author_name', ucfirst(trans('author_name')))->rules('required');
            $form->email('author_email', ucfirst(trans('author_email')))->rules('required');
            $form->text('title', ucfirst(trans('title')))->rules('required');

            $form->text('uri', ucfirst(trans('uri')))->rules('required');
            $form->script('page.testimony-stories.js');
            $form->textarea('desc', ucfirst(trans('description')))->rules('required');
            $form->ckeditor('content', ucfirst(trans('content')))->rules('required');
            $form->switch('publish_flg', ucfirst(trans('publish flg')))->rules('required');

            $form->display('created_at', ucfirst(trans('created_at')));
            $form->display('updated_at', ucfirst(trans('updated_at')));

            $form->saving(function (Form $form) {
                if($form->uri == ''){
                    $form->uri = strtolower(str_replace(' ', '-', $form->title));
                }
            });
        });
    }
}
