<?php

namespace App\Admin\Extensions\Form;
use Encore\Admin\Form\Field\PlainInput;
use Encore\Admin\Form\Field;

class InputCheckbox extends Field
{

    use PlainInput;
    public function render()
    {
        $this->initPlainInput();
        $this->prepend('')
            ->defaultAttribute('type', 'checkbox')
            ->defaultAttribute('id', $this->id)
            ->defaultAttribute('name', $this->elementName ?: $this->formatName($this->column))
            ->defaultAttribute('value', old($this->column, $this->value()))
            ->defaultAttribute('class', $this->getElementClassString())
            ->defaultAttribute('placeholder', $this->getPlaceholder());

        return parent::render()->with([
            'prepend' => $this->prepend,
            'append'  => $this->append,
        ]);
    }
}
