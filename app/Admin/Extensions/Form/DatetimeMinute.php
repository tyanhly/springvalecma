<?php

namespace App\Admin\Extensions\Form;

use Encore\Admin\Form\Field\Date as FormDate;

class DatetimeMinute extends FormDate
{
    protected $format = 'YYYY-MM-DD HH:mm';

    public function render()
    {
        $this->defaultAttribute('style', 'width: 160px');

        return parent::render();
    }
}
