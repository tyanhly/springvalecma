<?php

namespace App\Admin\Extensions\Form;

use Encore\Admin\Form\Field;

class CKEditor extends Field
{
    public static $js = [
        '//cdn.ckeditor.com/4.7.3/standard/ckeditor.js',
        '//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.7.3/adapters/jquery.js',
        '/packages/ckeditor-options.js',
    ];

    protected $view = 'admin.ckeditor';

    public function render()
    {
        $class = implode('.', $this->getElementClass());
        $this->script = "$('textarea.{$class}').ckeditor(ckoptions);";

        return parent::render();
    }
}