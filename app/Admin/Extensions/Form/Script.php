<?php

namespace App\Admin\Extensions\Form;

use Encore\Admin\Form\Field;

class Script extends Field
{
    public function render()
    {
        $js = '/js/admin/' . $this->column;
        return "<script type='text/javascript'>
                     jQuery.getScript('$js');
                </script>";
    }
} 