Please check the following instructions before submitting a bug :
 * Make sure you are using the latest version.
 * Make sure you read [installation](http://unisharp.github.io/filemanager/installation), [integration](http://unisharp.github.io/filemanager/integration), and [upgrade](http://unisharp.github.io/filemanager/upgrade) document.

If your problem still remains, please do these steps :
 * Run `php ./vendor/unisharp/filemanager/bin/debug` and paste the results here.
 * Capture screenshots of your browser console and paste them here.
 * Provide the steps to reproduce your issue, so we might solve it quicker.
