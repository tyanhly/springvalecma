## Steps to contribute
1. Fork [unisharp/filemanager](https://github.com/UniSharp/filemanager) from GitHub.
1. Run commands below:

    ```
    git clone git@github.com:UniSharp/filemanager-example-5.3.git
    cd filemanager-example-5.3
    composer require unisharp/filemanager:dev-master
    make init
    ```
1. Edit codes in `vendor/unisharp/filemanager`
1. Push your changes to your fork.
1. Send a pull request to [unisharp/filemanager](https://github.com/UniSharp/filemanager).
