[![Latest Stable Version](https://poser.pugx.org/unisharp/filemanager/v/stable)](https://packagist.org/packages/unisharp/filemanager)
[![Total Downloads](https://poser.pugx.org/unisharp/filemanager/downloads)](https://packagist.org/packages/unisharp/filemanager)
[![License](https://poser.pugx.org/unisharp/filemanager/license)](https://packagist.org/packages/unisharp/filemanager)

## Features
 * CKEditor and TinyMCE integration
 * Standalone button
 * Uploading validation
 * Cropping and resizing of images
 * Public and private folders for multi users
 * Customizable routes, middlewares, views, and folder path
 * Supports two types : files and images. Each type works in different directory.
 * Supported locales : ar, bg, de, el, en, es, fa, fr, he, hu, nl, pl, pt-BR, pt_PT, ro, ru, tr, zh-CN, zh-TW

PR is welcome!

## Screenshots
> Standalone button :

![Standalone button demo](https://unisharp.github.io/filemanager/images/lfm01.png)

> Grid view :

![Grid view demo](https://unisharp.github.io/filemanager/images/lfm02.png)

> List view :

![List view demo](https://unisharp.github.io/filemanager/images/lfm03.png)
  
