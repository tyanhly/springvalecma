<?php

namespace Unisharp\Laravelfilemanager\Handlers;

use Illuminate\Support\Facades\Auth;

class ConfigHandler
{
    public function userField()
    {
    	return Auth::guard('admin')->user()->id;
        // return auth()->user()->id;
    }
}
