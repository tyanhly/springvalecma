<?php

namespace Unisharp\Laravelfilemanager\controllers;

/**
 * Class DemoController.
 */
class DemoController extends LfmController
{
    /**
     * @return mixed
     */
    public function index()
    {
        return view('filemanager::demo');
    }
}
