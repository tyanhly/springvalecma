
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');
require('./constants');

import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import Main from 'Main';

import { Switch, Route } from 'react-router-dom';
import Index from 'pages/Index';
import Events from 'pages/Events';
import EventDetail from 'pages/EventDetail';
import Seremons from 'pages/Seremons';
import SeremonDetail from 'pages/SeremonDetail';
import TestimonyStories from 'pages/TestimonyStories';
import TestimonyStoryDetail from 'pages/TestimonyStoryDetail';
import Contact from 'pages/Contact';
render(

    <BrowserRouter>
        <Main >
            <Switch>
                <Route exact path='/' component={Index}/>
                <Route exact path='/events' component={Events}/>
                <Route path='/events/:uri' component={EventDetail}/>
                <Route exact path='/seremons' component={Seremons}/>
                <Route exact path='/seremons/:uri' component={SeremonDetail}/>
                <Route exact path='/testimony-stories' component={TestimonyStories}/>
                <Route path='/testimony-stories/:uri' component={TestimonyStoryDetail}/>
                <Route path='/contact' component={Contact}/>
            </Switch>
        </Main>
    </BrowserRouter>,
    document.getElementById('root'));
