/**
 * Utils
 */
export default class {
    static getLimitWords(str, limit){
        return str.split(' ', limit).join(' ');
    }
}