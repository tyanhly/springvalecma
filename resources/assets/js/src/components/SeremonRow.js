// TableRow.js

import Utils from 'common/Utils';
import React, { Component } from 'react';
import moment  from 'moment';
import { Link } from 'react-router-dom';

export default class SeremonRow extends Component {

    render() {
        return (
            <li>
                <div>
                    <h3 className="data-list-title">
                        <Link to={`/seremons/${encodeURI(this.props.obj.uri)}`}>{this.props.obj.title}</Link>
                    </h3>
                    <div className="data-record-meta">
                        <span >
                            <i className='fa fa-user'> </i>
                            { ' ' + this.props.obj.author_name }
                        </span>
                        <span>
                            <i className='fa fa-calendar'> </i>
                            {  ' ' + moment(this.props.obj.publish_at).format( 'YYYY-MM-DD') }
                        </span>
                    </div>
                    <div className='data-list-item-image' >
                        <img src={`uploads/${this.props.obj.image_uri}`}  />
                    </div>
                    <div className='data-list-item-desc data-record-text'>
                        <p>
                            {Utils.getLimitWords(this.props.obj.desc,50)}
                        </p>
                    </div>
                    <div className="clear-both"> </div>
                </div>
            </li>
        );
    }
};

