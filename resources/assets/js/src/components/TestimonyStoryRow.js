// TableRow.js

import Utils from 'common/Utils';
import React, { Component } from 'react';
import moment  from 'moment';
import { Link } from 'react-router-dom';

export default class TestimonyStoryRow extends Component {

    render() {
        return (
            <li><div>
                <h3 className="data-list-title">
                    <Link to={`/testimony-stories/${encodeURI(this.props.obj.uri)}`}>{this.props.obj.title}</Link>
                </h3>
                <div className="data-record-meta">
                    <span >
                         <i className='fa fa-user'> </i>
                        { ' ' + this.props.obj.author_name }
                    </span>
                    <span>
                        <i className='fa fa-calendar'> </i>
                        {  ' ' + moment(this.props.obj.created_at).format( 'YYYY-MM-DD') }
                    </span>
                </div>
                <div className='data-record-text'>
                    {Utils.getLimitWords(this.props.obj.desc,50)}
                </div>
            </div></li>
        );
    }
};

