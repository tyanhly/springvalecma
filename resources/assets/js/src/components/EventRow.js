// TableRow.js

import Utils from 'common/Utils';
import React, { Component } from 'react';
import moment  from 'moment';
import { Link } from 'react-router-dom';

export default class EventRow extends Component {

    render() {
        return (
            <li>
                <div>
                    <h3 className="data-list-title">
                        <Link to={`/events/${encodeURI(this.props.obj.uri)}`}>{this.props.obj.title}</Link>
                    </h3>

                    <div className="data-record-meta">
                        {this.props.obj.event_time && <span>
                            <i className='fa fa-calendar'></i>
                            { ' ' + moment(this.props.obj.event_time).format( 'YYYY-MM-DD hh:mm')}
                        </span>}
                    </div>
                    <div className='data-record-text'>

                        {Utils.getLimitWords(this.props.obj.desc,50)}

                    </div>
                </div>
            </li>
        );
    }
};

