import React, { Component } from 'react';

import Header from "./partials/Header";
import Footer from "./partials/Footer";

const Main = ({ match, children: Content }) => (

    <div id='body'>
        <Header match={match} />
        <section id='pageContent'>
            {Content}
        </section>
        <Footer />
    </div>
);

export default Main;