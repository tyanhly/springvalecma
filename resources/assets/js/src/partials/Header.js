import React from 'react';
import NavBar from './NavBar';

const Header = ({match}) =>
    <div className='container'>
        <div className='row'>
            <div className='col-md-12'>
                <header id='header'>
                    <div id='logo'>
                        <table >
                            <tbody>
                                <tr >
                                    <td >
                                        <img src={'/img/logo.png'} alt={'Springvale CMA'}/>
                                    </td>
                                    <td >
                                        <div id='logo-text'>
                                            <span >Springvale CMA </span>
                                            <span> Church</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id='nav'>
                        <NavBar match={match}/>
                    </div>
                </header>

            </div>
        </div>
    </div>;

export default Header;