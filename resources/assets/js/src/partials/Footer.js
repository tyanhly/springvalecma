import React, {Component} from 'react';

export default class Footer extends Component {
    render() {
        return (
            <div id='footerWrapper'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-md-12'>
                            <footer id='footer'>
                                <p>
                                    Copyright © 2017 Springvale CMA. All right reserved
                                </p>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
