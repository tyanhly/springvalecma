import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import { LinkContainer, IndexLinkContainer } from 'react-router-bootstrap';

const NavLink = ({ exact, to, eventKey, children }) =>
    <LinkContainer exact={exact} to={to} eventKey={eventKey}>
        <NavItem>{children}</NavItem>
    </LinkContainer>;

const MenuLink = ({ to, eventKey, children }) =>
    <LinkContainer to={to} eventKey={eventKey}>
        <MenuItem>{children}</MenuItem>
    </LinkContainer>;

const NavBar = ({ match }) =>
    <Navbar fluid collapseOnSelect>
        <Navbar.Header>
            <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav pullRight>
                <NavLink exact to='/' eventKey={6}>
                    Home
                </NavLink>
                <NavLink to='/events' eventKey={2}>
                    Events
                </NavLink>
                <NavLink to='/seremons' eventKey={3}>
                    Seremons
                </NavLink>
                <NavLink to='/testimony-stories' eventKey={4}>
                    Stories
                </NavLink>
                <NavLink to='/contact' eventKey={5}>
                    Contact
                </NavLink>
            </Nav>

        </Navbar.Collapse>
    </Navbar>;
export default NavBar;
