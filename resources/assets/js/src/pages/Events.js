import React, { Component } from 'react';

import axios from 'axios';
import EventRow from '../components/EventRow';
import Pagination  from 'react-js-pagination';

export default class Events extends  Component {
    constructor(props) {
        super(props);
        this.state = { items: [], pagination: {}};
        this.perPage = 5;
        this.getEventData = this.getEventData.bind(this);
    }

    componentWillMount(){
        this.getEventData(1);
    }

    getEventData(currentPage){
        var self = this;
        axios.get(`/api/events?page=${currentPage}&per_page=${this.perPage}`)
            .then(response => {
                self.setState({
                    items: response.data.data,
                    pagination: {
                        links: response.data.links,
                        meta: response.data.meta
                    }
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    renderEventRows(){
        return this.state.items.map( (object, i) => {
            return <EventRow obj={object} key={i} />;
        })

    }

    render(){
        return (
            <div className='container'>
                <div className='row'>
                    <div className="content col-md-12">
                        <h2 className="section-title">Events</h2>
                        <ul className="data-list large">
                            {this.renderEventRows()}
                        </ul>
                        {
                            this.state.pagination.meta &&
                            <Pagination
                                activePage={this.state.pagination.meta.current_page}
                                itemsCountPerPage={this.state.pagination.meta.per_page}
                                totalItemsCount={this.state.pagination.meta.total}
                                pageRangeDisplayed={3}
                                onChange={this.getEventData}
                            />
                            }
                    </div>
                </div>
            </div>
        )
    }
};