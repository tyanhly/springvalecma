import React, { Component } from 'react';

import axios from 'axios';
import moment from "moment/moment";
import { ShareButtons } from 'react-share';

const {
    FacebookShareButton,
    TwitterShareButton,
    GooglePlusShareButton

} = ShareButtons;
export default class SeremonDetail extends  Component {
    constructor(props) {
        super(props);
        this.state = { seremon: {}};
    }
    componentDidMount(){
        axios.get(`/api/seremons/${this.props.match.params.uri}`)
            .then(response => {
                this.setState({ seremon: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    render(){
        if(this.state.seremon.title) {
            return (
                <div className='container'>
                    <div className='row'>
                        <div className="content col-md-12">
                            <h2 className="section-title">{this.state.seremon.title}</h2>
                            <div className="data-record-meta">
                                <span className='pull-left'>
                                    <i className='fa fa-user'> </i>
                                    {' ' + this.state.seremon.author_name}
                                </span>
                                <span className='pull-center'>
                                    <i className='fa fa-calendar'> </i>
                                    {' ' + moment(this.state.seremon.publish_at).format('YYYY-MM-DD')}
                                </span>
                                <span className='social-share-buttons pull-right'>
                                    <FacebookShareButton
                                        url={window.CONST.APP_URL + '/seremons/' + this.state.seremon.uri}
                                        quote={window.CONST.APP_NAME + ' - ' + this.state.seremon.title + " - " + this.state.seremon.desc}
                                    >
                                        <i className="fa fa-facebook"> </i>
                                    </FacebookShareButton>
                                    <TwitterShareButton
                                        url={window.CONST.APP_URL + '/seremons/' + this.state.seremon.uri}
                                        via={this.state.seremon.email}
                                        title={window.CONST.APP_NAME + ' - ' + this.state.seremon.title + "\n" + this.state.seremon.desc + "\n"}
                                    >
                                        <i className="fa fa-twitter"> </i>
                                    </TwitterShareButton>
                                    <GooglePlusShareButton
                                        url={window.CONST.APP_URL + '/seremons/' + this.state.seremon.uri}
                                    >
                                        <i className="fa fa-google"> </i>
                                    </GooglePlusShareButton>
                                </span>
                            </div>
                            <div className='data-record-text'
                                 dangerouslySetInnerHTML={{__html: this.state.seremon.content}}/>
                        </div>
                    </div>
                </div>
            )
        } else {
            return null;
        }
    }

};