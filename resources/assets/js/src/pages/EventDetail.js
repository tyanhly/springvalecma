import React, { Component } from 'react';

import axios from 'axios';
import moment from "moment/moment";

import { ShareButtons } from 'react-share';

const {
    FacebookShareButton,
    TwitterShareButton,
    GooglePlusShareButton

} = ShareButtons;
export default class EventDetail extends  Component {
    constructor(props) {
        super(props);
        this.state = { event: {}};
    }
    componentDidMount(){
        axios.get(`/api/events/${this.props.match.params.uri}`)
            .then(response => {
                this.setState({ event: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    render(){
        return (
            <div className='container-fluid'>
                <div className='row'>
                    <div className="content col-md-12">
                        <h2 className="section-title">{this.state.event.title}</h2>
                        <div dangerouslySetInnerHTML={{__html: this.state.event.content}} />
                    </div>
                </div>
            </div>
        )
    }

    render(){
        if(this.state.event.title) {
            return (
                <div className='container'>
                    <div className='row'>
                        <div className="content col-md-12">
                            <h2 className="section-title">{this.state.event.title}</h2>
                            <div className="data-record-meta">
                                <span >
                                    <i className='fa fa-calendar'> </i>
                                    {' ' + moment(this.state.event.event_time).format('YYYY-MM-DD hh:mm')}
                                </span>
                                <span className='social-share-buttons pull-right'>
                                    <FacebookShareButton
                                        url={window.CONST.APP_URL + '/events/' + this.state.event.uri}
                                        quote={window.CONST.APP_NAME + ' - ' + this.state.event.title + " - " + this.state.event.desc}
                                    >
                                        <i className="fa fa-facebook"> </i>
                                    </FacebookShareButton>
                                    <TwitterShareButton
                                        url={window.CONST.APP_URL + '/events/' + this.state.event.uri}
                                        title={window.CONST.APP_NAME + ' - ' + this.state.event.title + "\n" + this.state.event.desc + "\n"}
                                    >
                                        <i className="fa fa-twitter"> </i>
                                    </TwitterShareButton>
                                    <GooglePlusShareButton
                                        url={window.CONST.APP_URL + '/events/' + this.state.event.uri}
                                    >
                                        <i className="fa fa-google"> </i>
                                    </GooglePlusShareButton>
                                </span>
                            </div>
                            <div className='data-record-text'
                                 dangerouslySetInnerHTML={{__html: this.state.event.content}}/>
                        </div>
                    </div>
                </div>
            )
        } else {
            return null;
        }
    }

};