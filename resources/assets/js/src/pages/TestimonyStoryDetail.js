import React, { Component } from 'react';

import axios from 'axios';
import moment from "moment/moment";
import { ShareButtons } from 'react-share';

const {
    FacebookShareButton,
    TwitterShareButton,
    GooglePlusShareButton

} = ShareButtons;

export default class TestimonyStoryDetail extends  Component {
    constructor(props) {
        super(props);
        this.state = { testimonyStory: {}};
    }
    componentDidMount(){
        axios.get(`/api/testimony-stories/${this.props.match.params.uri}`)
            .then(response => {
                this.setState({ testimonyStory: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    render(){
        if(this.state.testimonyStory.title) {
            return (
                <div className='container'>
                    <div className='row'>
                        <div className="content col-md-12">
                            <h2 className="section-title">{this.state.testimonyStory.title}</h2>
                            <div className="data-record-meta">
                                <span className='pull-left'>
                                    <i className='fa fa-user'> </i>
                                    {' ' + this.state.testimonyStory.author_name}
                                </span>
                                <span className='pull-center'>
                                    <i className='fa fa-calendar'> </i>
                                    {' ' + moment(this.state.testimonyStory.created_at).format('YYYY-MM-DD')}
                                </span>
                                <span className='social-share-buttons pull-right'>
                                    <FacebookShareButton
                                        url={window.CONST.APP_URL + '/testimony-stories/' + this.state.testimonyStory.uri}
                                        quote={window.CONST.APP_NAME + ' - ' + this.state.testimonyStory.title + " - " + this.state.testimonyStory.desc}
                                    >
                                        <i className="fa fa-facebook"> </i>
                                    </FacebookShareButton>
                                    <TwitterShareButton
                                        url={window.CONST.APP_URL + '/testimony-stories/' + this.state.testimonyStory.uri}
                                        via={this.state.testimonyStory.email}
                                        title={window.CONST.APP_NAME + ' - ' + this.state.testimonyStory.title + "\n" + this.state.testimonyStory.desc + "\n"}
                                    >
                                        <i className="fa fa-twitter"> </i>
                                    </TwitterShareButton>
                                    <GooglePlusShareButton
                                        url={window.CONST.APP_URL + '/testimony-stories/' + this.state.testimonyStory.uri}
                                    >
                                        <i className="fa fa-google"> </i>
                                    </GooglePlusShareButton>
                                </span>
                            </div>
                            <div className='data-record-text'
                                 dangerouslySetInnerHTML={{__html: this.state.testimonyStory.content}}/>
                        </div>
                    </div>
                </div>
            )
        } else {
            return null;
        }
    }
};