import React, { Component } from 'react';
import {Carousel} from 'react-bootstrap';
import axios from "axios/index";
import { Link } from 'react-router-dom';
import EventRow from 'components/EventRow';
import TestimonyStoryRow from "../components/TestimonyStoryRow";
import SeremonRow from "../components/SeremonRow";

export default class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            event: {items: []},

            testimonyStory: {items: []},

            seremon: {items: []}
        };
        this.getEventData = this.getEventData.bind(this);
        this.getTestimonyStoryData = this.getTestimonyStoryData.bind(this);
        this.getSeremonData = this.getSeremonData.bind(this);
    }



    render() {
        return (
            <div className='' id='home'>
                <div className='row'>
                    <div className="content col-md-12">
                        <Carousel interval={0}>
                            <Carousel.Item>
                                <div className='carousel-item-inner' style={{backgroundImage:'url(/img/home/carousel1.png)'}}>
                                    <Carousel.Caption>
                                        <h3>PLACE WITH A REAL LOVE</h3>
                                        <p>The place is church believes in Jesus, a church that loves God and people... </p>
                                    </Carousel.Caption>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <div className='carousel-item-inner' style={{backgroundImage:'url(/img/home/carousel3.png)'}}>
                                    <Carousel.Caption>
                                        <h3>YOU'RE WELCOME HERE</h3>
                                        <p>At the heart of who we are is a belief in belonging. God’s arms are wide enough and His love strong enough, no exceptions. So we join together and say, “Come home. You’re welcome here.”  </p>
                                    </Carousel.Caption>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <div className='carousel-item-inner' style={{ backgroundImage:'url(/img/home/carousel2.png)'}}>
                                    <Carousel.Caption>
                                        <h3>OUR COMMUNITY ACTIVITIES</h3>
                                        <p>No one stands alone. Being together, we sing to honor Chirst Jesus  </p>
                                    </Carousel.Caption>
                                </div>
                            </Carousel.Item>
                        </Carousel>
                    </div>
                </div>

                <div className='welcome'>
                    <div className='container'>
                        <div className='row'>
                            <div className="content col-md-12">
                                <div className='welcome-msg'>
                                    We love having guests! This coming weekend, we’d love to host you at one of our services. It’s our heart to make you feel right at home. We look forward to meeting you!
                                </div>
                                <hr className='welcome-hr'/>
                                <Link className="button welcome-contact-us" to="/contact" > Contact us </Link>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='home-testimony-stories'>
                    <div className='container'>
                        <div className='row'>
                            <div className="content col-md-12">
                                <h2 className="section-title">Our Stories</h2>
                                <ul className="data-list large">
                                    {this.renderTestimonyStoryRows()}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='home-seremons'>
                    <div className='container'>
                        <div className='row'>
                            <div className="content col-md-12">
                                <ul className="data-list large">
                                    {this.renderSeremonRows()}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div className=' home-events'>
                    <div className='container'>
                        <div className='row'>
                            <div className="content col-md-12">
                                <h2 className="section-title">Upcoming Events</h2>
                                <ul className="data-list large">
                                    {this.renderEventRows()}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }



    componentWillMount(){
        this.getEventData(1);
        this.getTestimonyStoryData(1);
        this.getSeremonData(1);
    }

    getSeremonData(currentPage){
        let self = this;
        axios.get(`/api/seremons?page=${currentPage}&per_page=4`)
            .then(response => {
                self.setState({
                    seremon: {
                        items: response.data.data
                    }
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    getTestimonyStoryData(currentPage){
        let self = this;
        axios.get(`/api/testimony-stories?page=${currentPage}&per_page=4`)
            .then(response => {
                self.setState({
                    testimonyStory: {
                        items: response.data.data
                    }
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    getEventData(currentPage){
        let self = this;
        axios.get(`/api/upcomming-events?page=${currentPage}&per_page=4`)
            .then(response => {
                self.setState({
                    event: {
                        items: response.data.data
                    }
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    renderEventRows(){
        return this.state.event.items.map( (object, i) => {
            return <EventRow obj={object} key={i} />
        })

    }
    renderSeremonRows(){
        return this.state.seremon.items.map( (object, i) => {
            return <SeremonRow obj={object} key={i} />
        })

    }
    renderTestimonyStoryRows(){
        return this.state.testimonyStory.items.map( (object, i) => {
            return <TestimonyStoryRow obj={object} key={i} />
        })

    }
}
