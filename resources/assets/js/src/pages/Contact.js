import React, { Component } from 'react';


export default class Contact extends Component {
    Contact(){

    }

    render() {
        let htmlFrame = `
         <iframe width="100%" height="450" frameborder="0" style="border: 1px solid #94d298;padding: 5px;background: #a6deb0;"
             src="https://www.google.com/maps/embed/v1/place?q=St+Johns+German+Lutheran+Church+3+Albert+Avenue+Springvale+VIC+Australia&key=${window.CONST.GOOGLE_KEY}"}
             allowfullscreen>

         </iframe>`;
        return (
            <div className='container'>
                <div className='row'>
                    <div className="col-md-12">
                        <h2 className="section-title">Contact Us</h2>
                    </div>
                </div>

                <div className='row' id='contactUs' >
                    <div className="col-md-12">
                        Need to get in touch? Find out how to contact us.
                        <br />
                        <br />
                    </div>
                    <div className="col-md-6">
                        <i className="fa fa-phone"> 00 000 000 00</i>
                    </div>
                    <div className="col-md-6">
                        <i className="fa fa-envelope-o"> springvalecma@gmail.com</i>
                    </div>
                    <div className="col-md-6">
                        <i className="fa fa-map-marker"> 3 Albert Ave, Springvale, VIC 3171</i>
                    </div>
                    <div className="col-md-6">
                        <i className="fa fa-facebook"> http://facebook.com/springvalecma</i>
                    </div>
                </div>

                <br />
                <div className='row'>
                    <div className="col-md-12">
                        <div
                            dangerouslySetInnerHTML={{__html: htmlFrame}}
                        />
                        <br />
                    </div>
                </div>
            </div>
        );
    }
}
