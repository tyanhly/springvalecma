import React, { Component } from 'react';

import axios from 'axios';
import TestimonyStoryRow from '../components/TestimonyStoryRow';
import Pagination  from 'react-js-pagination';

export default class TestimonyStories extends  Component {
    constructor(props) {
        super(props);
        this.state = { items: [], pagination: {}};
        this.perPage = 5;
        this.getData = this.getData.bind(this);
    }

    componentWillMount(){
        this.getData(1);
    }

    getData(currentPage){
        var self = this;
        axios.get(`/api/testimony-stories?page=${currentPage}&per_page=${this.perPage}`)
            .then(response => {
                self.setState({
                    items: response.data.data,
                    pagination: {
                        links: response.data.links,
                        meta: response.data.meta
                    }
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    renderRows(){
        return this.state.items.map( (object, i) => {
            return <TestimonyStoryRow obj={object} key={i} />;
        })

    }

    render(){
        return (
            <div className='container'>
                <div className='row'>
                    <div className="content col-md-12">
                        <h2 className="section-title">Testimony Stories</h2>
                        <ul className="data-list large">
                            {this.renderRows()}
                        </ul>
                        {
                            this.state.pagination.meta &&
                            <Pagination
                                activePage={this.state.pagination.meta.current_page}
                                itemsCountPerPage={this.state.pagination.meta.per_page}
                                totalItemsCount={this.state.pagination.meta.total}
                                pageRangeDisplayed={3}
                                onChange={this.getData}
                            />
                        }
                    </div>
                </div>
            </div>
        )
    }
};