echo '#========================='
echo '# Setup environment'
echo '#========================='
#apt-get update
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
apt-get install apt-get install htop php7.0* apache2 mysql-server -y
sudo apt-get remove php7.0-snmp
echo '#========================='
echo '# Config environment'
echo '#========================='

echo 'cd /springvalecma' >> /home/ubuntu/.bashrc
cp -rf /springvalecma/vagrant/os/* /
rm /etc/apache2/sites-enabled/*
ln -s /etc/apache2/sites-available/000localhost.conf /etc/apache2/sites-enabled/000localhost.conf 
a2enmod rewrite
service apache2 restart

echo '#========================='
echo '# Config project'
echo '#========================='
cd /springvalecma
mysql -uroot -proot  < /springvalecma/vagrant/create_db.sql
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
composer install
#wget https://getcomposer.org/download/1.5.5/composer.phar
#php composer.phar install
