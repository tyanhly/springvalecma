<?php

use Illuminate\Database\Seeder;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Auth\Database\Permission;
use Encore\Admin\Auth\Database\Menu;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
        	\Encore\Admin\Auth\Database\AdminTablesSeeder::class,
	    ]);


        $adminRole = Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
        ]);

        $arrPermissions = [
            [
                'name'        => 'Events',
                'slug'        => 'events',
                'http_method' => '',
                'http_path'   => '/events',
            ],
            [
                'name'        => 'Seremons',
                'slug'        => 'seremons',
                'http_method' => '',
                'http_path'   => '/seremons',
            ],
            [
                'name'        => 'Testimony TestimonyStories',
                'slug'        => 'testimony_stories',
                'http_method' => '',
                'http_path'   => '/testimony-stories',
            ],
            [
                'name'        => 'Pages',
                'slug'        => 'pages',
                'http_method' => '',
                'http_path'   => '/pages',
            ],
            [
                'name'        => 'Issues',
                'slug'        => 'issues',
                'http_method' => '',
                'http_path'   => '/issues',
            ],
            [
                'name'        => 'File Manager',
                'slug'        => 'filemanager',
                'http_method' => '',
                'http_path'   => '/filemanager',
            ],
            
        ];
        $permissionIds = [];
        foreach($arrPermissions as $permissionData){
        	$permissionIds[] = Permission::create($permissionData)->id;
        }
        $adminRole->permissions()->attach($permissionIds);
        
        $arrMenuItems = [
            [
                'parent_id' => 0,
                'order'     => 1,
                'title'     => 'Events',
                'icon'      => 'fa-newspaper-o',
                'uri'       => '/events',
            ],
            [
                'parent_id' => 0,
                'order'     => 2,
                'title'     => 'Seremons',
                'icon'      => 'fa-newspaper-o',
                'uri'       => '/seremons',
            ],
            [
                'parent_id' => 0,
                'order'     => 3,
                'title'     => 'Testimony TestimonyStories',
                'icon'      => 'fa-ticket',
                'uri'       => '/testimony-stories',
            ],
            [
                'parent_id' => 0,
                'order'     => 4,
                'title'     => 'Pages',
                'icon'      => 'fa-file-code-o',
                'uri'       => '/pages',
            ],
            [
                'parent_id' => 0,
                'order'     => 5,
                'title'     => 'Issues',
                'icon'      => 'fa-support',
                'uri'       => '/issues',
            ],
            [
                'parent_id' => 2,
                'order'     => 6,
                'title'     => 'File Manager',
                'icon'      => 'fa-folder',
                'uri'       => '/filemanager',
            ],
        ];

        foreach($arrMenuItems as $menuItemData){
			$menuItem = Menu::create($menuItemData);
			$menuItem->roles()->save($adminRole);
        }
    }
}
