<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeremonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seremons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('author_email');
            $table->string('author_name');
            $table->string('title');
            $table->string('uri');
            $table->text('desc');
            $table->text('content');
            $table->string('image_uri');
            $table->boolean('publish_flg');
            $table->dateTime('publish_at');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seremons');
    }
}
