<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonyStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimony_stories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('author_email');
            $table->string('author_name');
            $table->string('title');
            $table->string('uri');
            $table->text('desc');
            $table->text('content');
            $table->boolean('publish_flg');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimony_stories');
    }
}
